module.exports = function(grunt) {

    grunt.initConfig({
        jshint: {
            files: ['Gruntfile.js','resources/assets/angular/apps/**/*.js'],
            options: {
                globals: {
                    jQuery: true
                },
                reporterOutput: ''
            }
        },
        watch: {
            dev : {
                files: ['Gruntfile.js','resources/assets/**/*'],
                tasks: ['jshint','clean','uglify','copy','less'],
                options: {
                    atBegin: true
                }
            },
            min : {
                files: ['Gruntfile.js','resources/assets/angular/**/*.js'],
                tasks: ['jshint','clean','uglify','copy','less'],
                options: {
                    atBegin: true
                }
            }
        },

        copy: {
            dev: {
                cwd: 'resources/assets/',  // set working folder / root to copy
                src: ['**/*','.themes.json'],
                dest: 'public/',    // destination folder
                expand: true           // required when using cwd
            },

            live: {
                cwd: 'resources/assets/',  // set working folder / root to copy
                src: ['**/*','.themes.json', '!**/angular/apps/**', '!**/angular/modules/**', '!**/readme.md','**/*.html'],
                dest: 'public/',    // destination folder
                expand: true           // required when using cwd
            }
        },

        uglify: {
            app: {
                options: {
                    compress: {
                        drop_console: true,
                        warnings: true,
                        unused: true,
                        hoist_funs: true,
                        side_effects: false
                    }
                },
                files: {

                }
            }
        },

        less: {
            production: {
                files: [
                    {
                        options: {
                            compress: true,
                            ieCompat: true
                        },
                        src: 'resources/assets/themes/orange/less/custom.less',
                        dest: 'resources/assets/themes/orange/css/custom.css'
                    }
                ]
            }
        },

        clean: {
            folder: ['public/angular']
        }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.task.registerTask('handle-live-env', 'Remove env for live builds.', function() {
        if (grunt.file.exists('.env')) {
            grunt.file.delete('.env');
        }
    });

    grunt.task.registerTask('handle-dev-env', 'Remove env for live builds.', function() {
        if (grunt.file.exists('.env')) {
            grunt.file.delete('.env');
        }

        grunt.file.copy('.env.example', '.env');
    });

    grunt.registerTask('default', [
        'jshint',
        'uglify',
        'clean',
        'copy',
        // 'less',
        // 'handle-dev-env'
    ]);

    grunt.registerTask('build-dev', [
        'uglify',
        'clean',
        'copy:dev',
        'less',
        // 'handle-dev-env'
    ]);

    grunt.registerTask('build-live', [
        'uglify',
        'clean',
        'copy:live',
        'less',
        // 'handle-live-env'
    ]);
};