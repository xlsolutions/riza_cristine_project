<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable = [
        'name', 'description', 'user_id', 'is_featured', 'end_date'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
