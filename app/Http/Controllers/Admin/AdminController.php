<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Announcement;
use App\SiteSettings;
use App\Post;
use App\Video;
use App\VirtualTour;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('admin');
    }

    public function showDashboard()
    {
        return view('admin.dashboard');
    }

    public function showAnnouncements()
    {
        $arrAnnouncements = Announcement::with('user')
                                        ->orderBy('is_featured', 'DESC')
                                        ->orderBy('created_at', 'DESC')
                                        ->paginate(5);

        return view('admin.announcements')->with(['announcements' => $arrAnnouncements]);
    }

    public function showPosts()
    {
        $arrPosts = Post::with('user')->orderBy('created_at','DESC')->paginate(5);

        return view('admin.posts')->with(['posts' => $arrPosts]);
    }

    public function showVideos()
    {
        $arrVideos = Video::with('user')->orderBy('created_at','DESC')->paginate(5);

        return view('admin.videos')->with(['videos' => $arrVideos]);
    }

    public function showUsers()
    {
        $arrUsers = User::with('roles')->orderBy('created_at','DESC')->paginate(5);

        return view('admin.users')->with(['users' => $arrUsers]);
    }

    public function showSiteSettings()
    {

        $settings = (object)SiteSettings::all()->keyBy('settings_key')->toArray();

        return view('admin.site_settings',['settings' => $settings]);
    }

    public function showVirtualSettings()
    {

        $virtualTour = VirtualTour::orderBy('name','ASC')->paginate(5);

        return view('admin.virtual_tours',['virtual_tours' => $virtualTour]);
    }
}
