<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnnouncementFormRequest;
use App\Announcement;
use Mockery\Exception;
use Illuminate\Contracts\Logging\Log;

class AnnouncementResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('admin.add_announcement');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AnnouncementFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementFormRequest $request)
    {

        try
        {
            $announcement = new Announcement;

            $announcement->name = $request['name'];
            $announcement->handle = $this->slugify($request['name']);
            $announcement->description = $request['description'];
            $announcement->end_date = $request['end_date'];
            $announcement->user_id = Auth::user()->id;
            $announcement->is_featured = $request->get('featured', 0);

            $objFile = $request->file('featured');

            $strFilename = time().'.'.$objFile->getClientOriginalExtension();
            $strPath = 'images/featured';

            $objFile->move($strPath,$strFilename);

            $announcement->featured_image = $strPath.'/'.$strFilename;

            $announcement->save();

            return redirect(route('admin.announcements'));
        }
        catch (Exception $objE)
        {
            Log::error($objE);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement = Announcement::find($id);

        return response()->view('admin.edit_announcement',['announcement' => $announcement,'action' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnnouncementFormRequest $request, $id)
    {
        $announcement = Announcement::find($id);

        $announcement->name = $request['name'];
        $announcement->handle = $this->slugify($request['name']);
        $announcement->description = $request['description'];
        $announcement->end_date = $request['end_date'];
        $announcement->is_featured = $request->get('is_featured',0);

        if($request->hasFile('featured'))
        {
            $objFile = $request->file('featured');

            $strFilename = time().'.'.$objFile->getClientOriginalExtension();
            $strPath = 'images/featured';

            $objFile->move($strPath,$strFilename);

            $announcement->featured_image = $strPath.'/'.$strFilename;

        }

        $announcement->save();

        return redirect(route('admin.announcements'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Announcement::destroy($id);

        return redirect(route('admin.announcements'));
    }

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
