<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Http\Requests\PostFormRequest;

class PostResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('admin.add_post');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostFormRequest $request)
    {
        try
        {
            $post = new Post;

            $post->title = $request['title'];
            $post->handle = $this->slugify($request['title']);
            $post->description = $request['description'];
            $post->user_id = Auth::user()->id;

            $objFile = $request->file('featured');

            $strFilename = time().'.'.$objFile->getClientOriginalExtension();
            $strPath = 'images/featured';

            $objFile->move($strPath,$strFilename);

            $post->featured_image = $strPath.'/'.$strFilename;

            $post->save();

            return redirect(route('admin.posts'));
        }
        catch (Exception $objE)
        {
            Log::error($objE);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return response()->view('admin.edit_post',['post' => $post,'action' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        $post->title = $request['title'];
        $post->handle = $this->slugify($request['title']);
        $post->description = $request['description'];

        if($request->hasFile('featured'))
        {
            $objFile = $request->file('featured');

            $strFilename = time().'.'.$objFile->getClientOriginalExtension();
            $strPath = 'images/featured';

            $objFile->move($strPath,$strFilename);

            $post->featured_image = $strPath.'/'.$strFilename;

        }

        $post->save();

        return redirect(route('admin.posts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Post::destroy($id);

        return redirect(route('admin.posts'));
    }

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
