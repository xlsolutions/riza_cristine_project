<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserFormRequest;
use App\User;
use App\Role;
use App\UserRole;

class UserResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arrRoles = Role::get();

        return view('admin.add_user')->with(['roles' => $arrRoles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
        $strName = $request['lastname'].', '.$request['firstname'];
        $strPassword = $request['password'];
        $strEmail = $request['email'];

        $intRoleID= Role::where('handle',$request['role'])->first()->id;

        $user = new User;
        $user->name = $strName;
        $user->email = $strEmail;
        $user->password = bcrypt($strPassword);

        $user->save();

        $intUserID = $user->id;

        $userRole = new UserRole;
        $userRole->user_id = $intUserID;
        $userRole->role_id = $intRoleID;

        $userRole->save();

        return redirect(route('admin.users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo 'test';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id)->with('roles')->first();

        $name = explode(", ", $user->name);

        $arrUser = [];
        $arrUser['id'] = $user->id;
        $arrUser['firstname'] = $name[1];
        $arrUser['lastname'] = $name[0];
        $arrUser['email'] = $user->email;
        $arrUser['password'] = $user->password;

        foreach($user->roles as $role)
        {
            $arrUser['role'] = $role->name;
        }

        $arrRoles = Role::get();

        return response()->view('admin.edit_user',['user' => (object)$arrUser,'roles' => $arrRoles, 'action' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $strName = $request['lastname'].', '.$request['firstname'];
        $strEmail = $request['email'];
        $strPassword = $request['password'];

        $user = User::find($id);
        $user->name = $strName;
        $user->email = $strEmail;
        $user->password = bcrypt($strPassword);

        $user->save();

        return redirect(route('admin.users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect(route('admin.users'));
    }
}
