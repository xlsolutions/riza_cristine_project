<?php

namespace App\Http\Controllers\Admin;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('admin.add_video');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoFormRequest $request)
    {
        try
        {
            $video = new Video;

            $video->title = $request['title'];
            $video->handle = $this->slugify($request['title']);
            $video->description = $request['description'];
            $video->user_id = Auth::user()->id;

            $objFile = $request->file('featured');

            $strFilename = time().'.'.$objFile->getClientOriginalExtension();
            $strPath = 'videos';

            $objFile->move($strPath,$strFilename);

            $video->featured_video = $strPath.'/'.$strFilename;

            $video->save();

            return redirect(route('admin.videos'));
        }
        catch (Exception $objE)
        {
            Log::error($objE);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);

        return response()->view('admin.edit_video',['video' => $video,'action' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = Video::find($id);

        $video->title = $request['title'];
        $video->handle = $this->slugify($request['title']);
        $video->description = $request['description'];

        if($request->hasFile('featured'))
        {
            $objFile = $request->file('featured');

            $strFilename = time().'.'.$objFile->getClientOriginalExtension();
            $strPath = 'videos';

            $objFile->move($strPath,$strFilename);

            $video->featured_video = $strPath.'/'.$strFilename;

        }

        $video->save();

        return redirect(route('admin.videos'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Video::destroy($id);

        return redirect(route('admin.videos'));
    }

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
