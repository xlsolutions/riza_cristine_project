<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\VirtualTour;

class VirtualTourResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('admin.add_virtual_tour');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $virtual_tour = new VirtualTour();

            $virtual_tour->location = $request['location'];
            $virtual_tour->name = $request['name'];
            $virtual_tour->handle = $this->slugify($request['name']);
            $virtual_tour->latitude = $request['latitude'];
            $virtual_tour->longitude = $request['longitude'];

            if($request->hasFile('featured'))
            {
                $objFile = $request->file('featured');

                $strFilename = time().'.'.$objFile->getClientOriginalExtension();
                $strPath = 'images/featured';

                $objFile->move($strPath,$strFilename);

                $virtual_tour->featured_image = $strPath.'/'.$strFilename;

            }

            $virtual_tour->save();

            return redirect(route('admin.virtual-tours'));
        }
        catch (Exception $objE)
        {
            Log::error($objE);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $virtual_tour = VirtualTour::find($id);

        return response()->view('admin.edit_virtual_tour',['virtual_tour' => $virtual_tour,'action' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $virtual_tour = VirtualTour::find($id);

        $virtual_tour->location = $request['location'];
        $virtual_tour->name = $request['name'];
        $virtual_tour->handle = $this->slugify($request['name']);
        $virtual_tour->longitude = $request['longitude'];
        $virtual_tour->latitude = $request['latitude'];

        if($request->hasFile('featured'))
        {
            $objFile = $request->file('featured');

            $strFilename = time().'.'.$objFile->getClientOriginalExtension();
            $strPath = 'images/featured';

            $objFile->move($strPath,$strFilename);

            $virtual_tour->featured_image = $strPath.'/'.$strFilename;

        }

        $virtual_tour->save();

        return redirect(route('admin.virtual-tours'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
