<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function viewPost($handle)
    {
        $post = Post::where('handle',$handle)->first();

        return view('client.view_post')->with('post',$post);
    }
}
