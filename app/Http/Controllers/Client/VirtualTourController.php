<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\VirtualTour;

class VirtualTourController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function viewVirtualTour($handle)
    {
        $virtualTour = VirtualTour::where('handle',$handle)->first();

        return view('client.view_virtual_tour')->with('virtual_tour',$virtualTour);
    }
}
