<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\User;
use App\SiteSettings;
use App\Post;
use App\VirtualTour;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $currentUser;
    protected $siteSettings;
    protected $posts;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->siteSettings = (object)SiteSettings::all()->keyBy('settings_key')->toArray();

            $this->posts = (object)Post::orderBy('created_at','DESC')->take(4)->get();

            $this->virtualTour = VirtualTour::orderBy('name','ASC')->paginate(5);

            if(Auth::check())
            {
                $this->currentUser = Auth::user();
                View::share([
                    'currUser' => Auth::user(),
                    'isAdmin' => $this->isAdmin(),
                    'siteSettings' => $this->siteSettings,
                    'posts' => $this->posts,
                    'virtual_tours' => $this->virtualTour,
                ]);
            }else{
                View::share([
                    'siteSettings' => $this->siteSettings,
                    'posts' => $this->posts,
                    'virtual_tours' => $this->virtualTour,
                ]);
            }

            return $next($request);
        });
    }

    public function isAdmin()
    {
        if(Auth::check()){
            $user = User::find(Auth::user()->id);

            return $user->roles()->where('handle','ADMINISTRATOR')->exists();
        }

        return false;
    }

}
