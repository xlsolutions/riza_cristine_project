<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Video;
use App\VirtualTour;


class HomeController extends Controller
{

    private $objUser;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements = Announcement::orderBy('is_featured', 'DESC')
                                    ->orderBy('end_date', 'DESC')
                                    ->get();

        $videos = Video::orderBy('created_at', 'DESC')->get();

        return view('parallax',compact(['announcements','videos']));
    }

    public function showVirtualTour()
    {
        $virtualTours = VirtualTour::orderBy('name','ASC')->paginate(5);

        return view('client.virtual')->with('virtual_tours',$virtualTours);
    }
}
