<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $user = User::find(Auth::user()->id);

            if (Auth::user() &&  $user->roles()->where('handle','ADMINISTRATOR')->exists()) {
                return $next($request);
            }
        }

        return redirect('/');
    }
}
