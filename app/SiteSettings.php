<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class SiteSettings extends Model
{
    use Notifiable;

    protected $fillable = [
        'settings_key', 'settings_name','settings_value'
    ];
}
