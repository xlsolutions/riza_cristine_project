<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VirtualTour extends Model
{
    protected $fillable = ['location','name','longitude','latitude'];

}
