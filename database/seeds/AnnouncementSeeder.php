<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('announcements')->insert([
            'name' => 'Playa Laiya',
            'handle' => strtolower(str_replace(' ', '_', 'Playa Laiya')),
            'description' => '<p><strong>A QUALITY INVESTMENT and QUALITY TIME YOU CAN SHARE TO YOUR FAMILY! </strong><br /><strong>Have your Own Exclusive Beach Lot only here</strong> <br />@ Playa Laiya Juan, Batangas</p>',
            'featured_image' => 'images/featured/playa_laiya_1.png',
            'is_featured' => 1,
            'user_id' => 1,
            'end_date' => Carbon::now()->addWeeks(2)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
