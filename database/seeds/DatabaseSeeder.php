<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(RoleSeeder::class);
         $this->call(SiteSettingsSeeder::class);
         $this->call(VirtualTourSeeder::class);
         $this->call(PostSeeder::class);
         $this->call(AnnouncementSeeder::class);
         $this->call(VideoSeeder::class);
    }
}
