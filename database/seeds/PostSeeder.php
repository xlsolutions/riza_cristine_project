<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title' => 'Playa Laiya',
            'handle' => strtolower(str_replace(' ', '-', 'Playa Laiya')),
            'description' =>'<p>At Playa Laiya Beach Club, we make things convenient, comfortable and enjoyable from the reception all the way to a full day resort experience.</p>'.
                            '<p><strong><em>Welcome Center</em></strong></p>'.
                            '<p>Beautiful beginnings start with warm and hospitable reception. Get acquainted with the exciting offerings of Playa Laiya Beach Club at the Welcome Center.</p>'.
                            '<p><strong><em>The Sands Restaurant</em></strong></p>'.
                            '<p>The resort experience can only be fully enjoyed with satisfying dining in the mix. Sample your favorite tasty dishes and incredibly sumptuous meals at The Sands</p>'.
                            '<p><strong><em>Pool Pavilion</em></strong></p>'.
                            '<p>Relax and enjoy the view of the mountains and the sea at the majestic Pool Pavilion. Converge at Playa Laiya Beach Club&rsquo;s architectural prowess for a much more rewarding experience.</p>'.
                            '<p><strong><em>Infinity Pool</em></strong></p>'.
                            '<p>Nothing beats the feeling of freely wading into the waters and rising from it with the breathtaking view of the ocean to greet you. Savor the 360-degree view of Playa Laiya Beach Club&rsquo;s enchanting beauty.</p>'.
                            '<p><strong><em>Kiddie Pool</em></strong></p>'.
                            '<p>Let the little ones enjoy swimming safely. Have a worry-free fun under the sun that&rsquo;s both available for the kids and the kids at heart.</p>'.
                            '<p><strong><em>Picnic Tables</em></strong></p>'.
                            '<p>Enjoying the beach and the pool isn&rsquo;t just all about the waters, have a great time with loved ones at the picnic tables and create moments of togetherness.</p>'.
                            '<p><strong><em>Beach and Pool Cabanas</em></strong></p>'.
                            '<p>Playa Laiya Beach Club offers an es</p>'.
                            '<p>&bull; Big Cabana (accommodates 15 &ndash; 20 pax)<br />&bull; Pool Cabana and Beach Front Cabana (accommodates 6 -10 pax)<br />&bull; Pool Pavilion (accommodates 20 - 30 pax)<br />&bull; Marketing Pavilion (accommodates up to 100 pax)</p>'.
                            '<p>Escape from the heat, a comfortable shade, and a convenient space for a quick break from swimming and active resort experience with the beach and pool cabanas. Here are the variety of choices that fits your need:</p>',
            'featured_image' => 'images/featured/playa_laiya_1.png',
            'user_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
