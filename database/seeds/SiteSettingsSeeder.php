<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SiteSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_settings')->insert([
            [
                'settings_key' => 'site_name',
                'settings_name' => 'Site Name',
                'settings_value' => 'Playa Laiya Beachlot',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'settings_key' => 'site_address',
                'settings_name' => 'Site Address',
                'settings_value' => 'Laiya, San Juan, Batangas',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'settings_key' => 'site_email',
                'settings_name' => 'Site Email',
                'settings_value' => 'playalaiyabeachlot@gmail.com',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'settings_key' => 'site_contact',
                'settings_name' => 'Site Contact',
                'settings_value' => '------------',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'settings_key' => 'site_footer',
                'settings_name' => 'Site Footer',
                'settings_value' => 'XLSolutions',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'settings_key' => 'site_contact_intro',
                'settings_name' => 'Site Contact Intro ',
                'settings_value' => 'LIFE IN COLOR',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'settings_key' => 'site_subtitle',
                'settings_name' => 'Site Subtitle',
                'settings_value' => 'EXPERIENCE THE EXCITING AND COMPLETE BEACH SIDE LIFESTYLE',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'settings_key' => 'site_promo',
                'settings_name' => 'Site Promo',
                'settings_value' => 'INVEST NOW HAVE YOUR OWN BEACH LOT!',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
