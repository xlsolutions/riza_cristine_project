<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert(
            [
                [
                    'title' => 'Playa Laiya Video',
                    'handle' => strtolower(str_replace(' ', '-', 'Playa Laiya Video')),
                    'description' => '',
                    'featured_video' => 'videos/playa_laiya_video.mp4',
                    'user_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Own A Piece Of Laiya 2',
                    'handle' => strtolower(str_replace(' ', '-', 'Own A Piece Of Laiya 2')),
                    'description' => '',
                    'featured_video' => 'videos/own_a_piece_of_laiya_2.mp4',
                    'user_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Playa Laiya Health Benefit of the Sea',
                    'handle' => strtolower(str_replace(' ', '-', 'Playa Laiya Health Benefit of the Sea')),
                    'description' => '',
                    'featured_video' => 'videos/playa_laiya_health_benefit_of_the_sea.mp4',
                    'user_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Beach Life Style North Prospects',
                    'handle' => strtolower(str_replace(' ', '-', 'Beach Life Style North Prospects')),
                    'description' => '',
                    'featured_video' => 'videos/beach_life_style_north_prospects.mp4',
                    'user_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]
            ]
            );
    }
}
