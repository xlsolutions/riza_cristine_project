<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class VirtualTourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('virtual_tours')->insert([
            'name' => 'Playa Laiya',
            'location' => 'Playa Laiya',
            'handle' => strtolower(str_replace(' ', '-', 'Playa Laiya')),
            'longitude' => '121.394302',
            'latitude' => '13.673040',
            'featured_image' => 'images/featured/playa_laiya_1.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
