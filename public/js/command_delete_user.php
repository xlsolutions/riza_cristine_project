<?

function delete_user($inputstruct){
	global $uc_db_user , $uc_db_pass , $uc_db_host , $uc_database , $method_namespace , $log_file , $okay_string , $pref_attr ;
	global $debug , $setclass ;
	global $faultstring , $faultcode , $faultactor  , $faultdetail ;


	$dsn = "mysql://".$uc_db_user.":".$uc_db_pass."@".$uc_db_host."/".$uc_database ;
	$mdb2 = MDB2::connect($dsn);

	if (check_db_error($mdb2)) {
		log_it(__FUNCTION__.": SOAPFAULT :" . $faultcode . " - " . $faultstring . " - ".$faultdetail );
		return new SOAPFault($faultstring,$faultcode,$faultactor,$faultdetail);
	}
	if ($debug) {

		if (PEAR::isError($mdb2)) exit();

		$my_debug_handler = new Explain_Queries($mdb2);

		$mdb2->setOption('debug', 1);

		$mdb2->setOption('debug_handler',array($my_debug_handler, 'collectInfo'));


		register_shutdown_function( array($my_debug_handler, 'dumpInfo'));

	}


	
	if (is_object($setclass?$inputstruct[0]:$inputstruct)) {
		
			if (get_class($setclass?$inputstruct[0]:$inputstruct) != "DeleteUserStruct_datatype" ) {
				
				$faultcode = 'Client';
				$faultstring ="	Not an DeleteUserStruct" ;
				$faultactor = $method_namespace;
				$faultdetail = "";	
				log_it(__FUNCTION__.": SOAPFAULT :" . $faultcode . " - " . $faultstring . " - ".$faultdetail );	
				return new SOAPFault($faultcode,$faultstring,$faultactor,$faultdetail);
				
	}

	}

		
	if ($debug==1 ) {

			$res = print_r($inputstruct , true )	;
			log_it("INPUT_OBJECT :::".$res."\n");	

	}


	if ($setclass==0){
	
			if (($fault = $inputstruct->validate()) != 0 ) {

				return $fault ; 

			}
	
	}
	

	if ($setclass==1){
	
			if (($fault = $inputstruct[0]->validate()) != 0 ) {

				return $fault ; 

			}
	
	}	
	
	
	
	
	
	$struct=object_to_array($inputstruct);
	
	if ($setclass == 0 ) {
 	// when using setclass + __call() this function will get as input an array , not an object 
	$foo[] = $struct ;
	unset ($struct) ;
	$struct = $foo;	
	}
	

	$sipuser  = $struct[0]['key']['sipuser'];

	$sipdomain  = $struct[0]['key']['sipdomain'];



	// check for valid user


	$mdb2->setFetchMode(MDB2_FETCHMODE_ASSOC);

	$query = 'select uid from '.subscriber_table.' where username="'.$sipuser.'" and domain="'.$sipdomain.'"';

	$uid = $mdb2->queryOne($query);


	if (!is_numeric($uid)){

		$faultcode = 'Client';

		$faultstring = 'DataBase';

		$faultactor = $method_namespace;

		$faultdetail = "No such user" ;

		log_it(__FUNCTION__.": SOAPFAULT :" . $faultcode . " - " . $faultstring . " - ".$faultdetail );

		return new SOAPFault($faultcode,$faultstring,$faultactor,$faultdetail);

	}


	if ($mdb2->supports('transactions')) {
		$mdb2->beginTransaction();
	}



	$query = "delete from ".subscriber_table." where uid=".$uid;

	$result = $mdb2->exec($query);
	if (check_db_error($result)) {
		log_it(__FUNCTION__.": SOAPFAULT :" . $faultcode . " - " . $faultstring . " - ".$faultdetail );
		return new SOAPFault($faultstring,$faultcode,$faultactor,$faultdetail);		
	}


	$query = "delete from ".dbaliases." where username='".$sipuser. "' and domain='".$sipdomain."'" ;

	$result = $mdb2->exec($query);
	if (check_db_error($result)) {
		log_it(__FUNCTION__.": SOAPFAULT :" . $faultcode . " - " . $faultstring . " - ".$faultdetail );
		return new SOAPFault($faultstring,$faultcode,$faultactor,$faultdetail);	
	}



	$arr = array_values($pref_attr)  ;

	for ( $i=0 ; $i < count($arr) ; $i++)	{

		$p=explode(":" , $arr[$i] );

		$pref_table=$p[0];

		$query = "delete from ".$pref_table." where uid=".$uid;

		$result = $mdb2->exec($query);
		if (check_db_error($result)) {
			log_it(__FUNCTION__.": SOAPFAULT :" . $faultcode . " - " . $faultstring . " - ".$faultdetail );
			return new SOAPFault($faultcode,$faultstring,$faultactor,$faultdetail);	
		}

	}


	$query = "delete from ".speed_dial_table." where username='".$sipuser. "' and domain='".$sipdomain."'" ;

	$result = $mdb2->exec($query);
	if (check_db_error($result)) {
		log_it(__FUNCTION__.": SOAPFAULT :" . $faultcode . " - " . $faultstring . " - ".$faultdetail );
		return new SOAPFault($faultstring,$faultcode,$faultactor,$faultdetail);	
	}


	
	if ($mdb2->in_transaction) {

		$mdb2->commit();

	}

	$arrMQData = array('username' => $sipuser, 'domain' => $sipdomain);
	mq_send('soappi.subscriber.delete', serialize($arrMQData));


	return $okay_string ;


}


?>
