$(document).ready(function(){

    $("#mainNav ul a, .logo a, .shortLink a, .down a, .notBtn a, .shortContact a, .moreBtn a").click(function(e){


        var full_url = this.href;
        var parts = full_url.split("#");
        var trgt = parts[1];
        var target_offset = $("#"+trgt).offset();
        var target_top = target_offset.top;

        $('html,body').animate({scrollTop:target_top -110}, 800);
        return false;

    });

});
