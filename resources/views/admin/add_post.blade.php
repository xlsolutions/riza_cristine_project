@extends('layouts.admin')
@section('title', 'Add Post')
@section('content')
    <h3><i class="fa fa-tags"></i> Posts</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="form-panel">
                <h4 class="mb"><span class="fa fa-user"></span> Add Post</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li class="alert alert-danger">
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
                {{Form::open(['url' => route('post.store'), 'class' => 'form-horizontal style-form', 'method' => 'post','files' => true])}}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-9">
                        {{Form::text('text',Input::old('title'),['name' => 'title', 'id' => 'name', 'class' => 'form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        {{Form::textarea('text',Input::old('description'),['name' => 'description', 'id' => 'description', 'class' => 'form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 control-label">Set Featured Image</div>
                    <div class="col-md-10">
                        {{Form::file('featured',array('class' => 'file','id' => 'imgInp'))}}
                    </div>
                    <div class="col-md-5 top-buffer">
                        <img id="img-upload" style='width:100%;'>
                    </div>
                </div>
                <div class="form-group">
                </div>
                <button type="submit" class="btn btn-success"> Add Post</button>
                {{form::close()}}
            </div>
        </div>
    </div>
@stop
