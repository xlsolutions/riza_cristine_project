@extends('layouts.admin')
@section('title', 'Add User')
@section('content')
    <h3><i class="fa fa-users"></i> Users</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="form-panel">
                <h4 class="mb"><span class="fa fa-user"></span> Add User</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li class="alert alert-danger">
                        {{ $error }}
                        </li>
                    @endforeach
                </ul>
                {{Form::open(['url' => route('user.store'), 'class' => 'form-horizontal style-form', 'method' => 'post'])}}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-1 col-sm-1 control-label">First Name</label>
                        <div class="col-sm-3">
                            {{Form::text('text',Input::old('firstname'),['name' => 'firstname', 'id' => 'firstname', 'class' => 'form-control'])}}
                        </div>
                        <label class="col-sm-1 col-sm-1 control-label">Last Name</label>
                        <div class="col-sm-3">
                            {{Form::text('text',Input::old('lastname'),['name' => 'lastname', 'id' => 'lastname', 'class' => 'form-control'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1 col-sm-1 control-label">Email</div>
                        <div class="col-sm-7">
                            {{Form::email('email',Input::old('email'),['name' => 'email', 'id' => 'email', 'class' => 'form-control'])}}

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1 col-sm-1 control-label">Password</div>
                        <div class="col-sm-3">
                            {{Form::password('password',['name' => 'password', 'id' => 'password', 'class' => 'form-control'])}}
                        </div>
                        <div class="col-sm-1 col-sm-1 control-label">Confirm Password</div>
                        <div class="col-sm-3">
                            {{Form::password('password_confirmation',['name' => 'password_confirmation', 'id' => 'password_confirmation', 'class' => 'form-control'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1 col-sm-1 control-label">Role</div>
                        <div class="col-md-3">
                            <select multiple name="role" class="form-control">
                                @foreach($roles as $role)
                                    <option value="{{$role->handle}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                            <span class="help-block">Press control to select multiple roles.</span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">Add User</button>
                {{form::close()}}
            </div>
        </div>
    </div>
@stop
