@extends('layouts.admin')
@section('title', 'Add Virtual Tour')
@section('content')
    <h3><i class="fa fa-map-marker"></i> Virtual Tour</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="form-panel">
                <h4 class="mb"><span class="fa fa-map-marker"></span> Add Virtual Tour</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li class="alert alert-danger">
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
                {{Form::open(['url' => route('virtual-tour.store'), 'class' => 'form-horizontal style-form', 'method' => 'post','files' => true])}}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Location</label>
                    <div class="col-sm-9">
                        {{Form::text('text',Input::old('name'),['name' => 'location', 'id' => 'location', 'class' => 'form-control','placeholder' => 'Enter a location'])}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="map" id="map" style="width: 100%; height: 300px;"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-9">
                        {{Form::text('text',Input::old('name'),['name' => 'name', 'id' => 'name', 'class' => 'form-control','required'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Longitude</label>
                    <div class="col-sm-9">
                        {{Form::text('text',Input::old('longitude'),['name' => 'longitude', 'id' => 'longitude', 'class' => 'form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Latitude</label>
                    <div class="col-sm-9">
                        {{Form::text('text',Input::old('latitude'),['name' => 'latitude', 'id' => 'latitude', 'class' => 'form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 control-label">Set Featured Image</div>
                    <div class="col-md-10">
                        {{Form::file('featured',array('class' => 'file','id' => 'imgInp'))}}
                    </div>
                    <div class="col-md-5 top-buffer">
                        <img id="img-upload">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Update Post</button>
                {{form::close()}}
            </div>
        </div>
    </div>
@stop
