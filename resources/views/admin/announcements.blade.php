@extends('layouts.admin')
@section('title', 'Announcements')
@section('content')
    <h3><i class="fa fa-bullhorn"></i> Announcements</h3>
    <div class="row mt">
        <div class="col-md-12">
            <div class="form-panel">
                <section id="no-more-tables">
                    <h4><i class="fa fa-table"></i> Announcement Lists <a href="{{route('announcement.index')}}" class="btn btn-success pull-right"><span class="fa fa-plus"></span> Add new Announcement</a></h4>
                    <hr>
                    @if(count($announcements) > 0)
                        <table class="table table-striped table-condensed cf">
                            <thead class="cf">
                            <tr>
                                <th></th>
                                <th><i class="fa fa-bullhorn"></i> Name</th>
                                <th><i class="fa fa-question-circle"></i> Description</th>
                                <th><i class="fa fa-user"></i> Author</th>
                                <th><i class="fa fa-bookmark"></i> Featured</th>
                                <th><i class=" fa fa-calendar"></i> End Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($announcements as $announcement)
                                <tr>
                                    <td class="centered">
                                        <a href = "{{route('announcement.edit',$announcement->id)}}">
                                            <img src="{{$announcement->featured_image ? asset($announcement->featured_image) : asset('images/default/default.gif')}}" alt="{{$announcement->name}}" class="img-circle" height="42" width="42">
                                        </a>
                                    </td>
                                    <td data-title="Name"><a href="{{route('announcement.edit',$announcement->id)}}">{{$announcement->name}}</a></td>
                                    <td data-title="Description">{!! str_limit($announcement->description,150,'...') !!}</td>
                                    <td data-title="Author">{{str_limit($announcement->user->name)}}</td>
                                    <td data-title="Featured">
                                <span>
                                    <i class="fa {{$announcement->is_featured === 1 ? 'fa-check-circle' : 'fa-times-circle'}}"></i>
                                </span>
                                    </td>
                                    <td data-title="End Date">{{date('M d, Y, D',strtotime($announcement->end_date))}}</td>
                                    <td>
                                        <a href="{{route('announcement.edit',$announcement->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil "></i></a>
                                        @if(count($announcements) > 1)
                                            <a href="{{route('announcement.destroy',$announcement->id)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>No Results Found</p>
                    @endif
                </section>
            </div>
        </div>
    </div>
@stop
