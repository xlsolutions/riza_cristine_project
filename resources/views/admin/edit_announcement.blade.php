@extends('layouts.admin')
@section('title', 'Edit Announcement')
@section('content')
    <h3><i class="fa fa-bullhorn"></i> Announcements</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="form-panel">
                <h4 class="mb"><span class="fa fa-user"></span> {{$announcement->id ? 'Edit' : 'Add'}} Announcement</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li class="alert alert-danger">
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
                {{Form::open(['url' => $announcement->id ? route('announcement.update',$announcement->id) : route('announcement.store'), 'class' => 'form-horizontal style-form', 'method' => $announcement->id ? 'put' : 'post','files' => true])}}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-9">
                        {{Form::text('text',$announcement->name ? $announcement->name : Input::old('name'),['name' => 'name', 'id' => 'name', 'class' => 'form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        {{Form::textarea('text',$announcement->description ? $announcement->description : Input::old('description'),['name' => 'description', 'id' => 'description', 'class' => 'form-control','maxlength' => '255'])}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 control-label">End Date</div>
                    <div class="col-sm-3">
                        {{Form::date('text',$announcement->end_date ? date('Y-m-d',strtotime($announcement->end_date)) : Input::old('end_date'),['name' => 'end_date', 'id' => 'end_date', 'class' => 'form-control'])}}
                    </div>
                    <div class="col-sm-1 control-label">Featured</div>
                    <div class="col-sm-3">
                        {!! Form::checkbox('is_featured',1,$announcement->is_featured === 1,array('data-toggle' => 'switch')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 control-label">Set Featured Image</div>
                    <div class="col-md-10">
                        {{Form::file('featured',array('class' => 'file','id' => 'imgInp'))}}
                    </div>
                    <div class="col-md-5 top-buffer">
                        <img id="img-upload" src="{{asset($announcement->featured_image)}}" style='width:100%;' alt="{{$announcement->name}}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Update Announcement</button>
                {{form::close()}}
            </div>
        </div>
    </div>
@stop
