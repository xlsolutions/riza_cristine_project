@extends('layouts.admin')
@section('title', 'Edit Post')
@section('content')
    <h3><i class="fa fa-tags"></i> Posts</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="form-panel">
                <h4 class="mb"><span class="fa fa-user"></span> Edit Post</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li class="alert alert-danger">
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
                {{Form::open(['url' => $post->id ? route('post.update',$post->id) : route('post.store'), 'class' => 'form-horizontal style-form', 'method' => $post->id ? 'put' : 'post','files' => true])}}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-9">
                        {{Form::text('text',$post->title ? $post->title : Input::old('title'),['name' => 'title', 'id' => 'title', 'class' => 'form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        {{Form::textarea('text',$post->description ? $post->description : Input::old('description'),['name' => 'description', 'id' => 'description', 'class' => 'form-control','maxlength' => '255'])}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 control-label">Set Featured Image</div>
                    <div class="col-md-10">
                        {{Form::file('featured',array('class' => 'file','id' => 'imgInp'))}}
                    </div>
                    <div class="col-md-5 top-buffer">
                        <img id="img-upload" src="{{asset($post->featured_image)}}" style='width:100%;' alt="{{$post->name}}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Update Post</button>
                {{form::close()}}
            </div>
        </div>
    </div>
@stop
