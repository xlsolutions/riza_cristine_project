@extends('layouts.admin')
@section('title', 'Posts')
@section('content')
    <h3><i class="fa fa-tags"></i> Posts</h3>
    <div class="row mt">
        <div class="col-md-12">
            <div class="form-panel">
                <section id="no-more-tables">
                    <h4><i class="fa fa-table"></i> Post Lists <a href="{{route('post.index')}}" class="btn btn-success pull-right"><span class="fa fa-plus"></span> Add New Post</a></h4>
                    <hr>
                    @if(count($posts) > 0)
                        <table class="table table-striped table-condensed cf">
                            <thead class="cf">
                            <tr>
                                <th></th>
                                <th><i class="fa fa-bullhorn"></i> Title</th>
                                <th><i class="fa fa-question-circle"></i> Description</th>
                                <th><i class="fa fa-user"></i> Author</th>
                                <th><i class=" fa fa-calendar"></i> Date Created</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td class="centered">
                                        <a href="{{route('post.edit',$post->id)}}">
                                            <img src="{{$post->featured_image ? asset($post->featured_image) : asset('images/default/default.gif')}}" alt="{{$post->title}}" class="img-circle" height="42" width="42">
                                        </a>
                                    </td>
                                    <td data-title="Title"><a href="{{route('post.edit',$post->id)}}">{{$post->title}}</a></td>
                                    <td data-title="Description">{!! str_limit($post->description,100,'...') !!}</td>
                                    <td data-title="Author">{{str_limit($post->user->name)}}</td>
                                    <td data-title="Date Created">{{date('M d, Y, D',strtotime($post->created_at))}}</td>
                                    <td>
                                        <a href="{{route('post.edit',$post->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil "></i></a>
                                        @if(count($posts) > 1)
                                            <a href="{{route('post.destroy',$post->id)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>No Results Found</p>
                    @endif
                </section>
            </div>
        </div>
    </div>
@stop
