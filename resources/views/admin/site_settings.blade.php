@extends('layouts.admin')
@section('title', 'Site Settings')
@section('content')
    <h3><i class="fa fa-gear"></i> Settings</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="form-panel">
                <h4 class="mb"><span class="fa fa-gears"></span> Site Settings</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li class="alert alert-danger">
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
                {{Form::open(['url' => route('site-setting.store'), 'class' => 'form-horizontal style-form', 'method' => 'post'])}}
                @foreach($siteSettings as $setting)
                    <div class="form-group">
                        <label class="col-md-2 col-sm-12 control-label">{{$setting['settings_name']}}</label>
                        <div class="col-sm-10">
                            {{Form::text('text',$setting['settings_value'],['name' => $setting['settings_key'], 'id' => $setting['settings_key'], 'class' => 'form-control','required' => 'required'])}}
                        </div>
                    </div>
                @endforeach

                <button type="submit" class="btn btn-success">Update Settings</button>
                {{Form::close()}}
            </div>
        </div>
    </div>
@stop
