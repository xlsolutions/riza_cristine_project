@extends('layouts.admin')
@section('title', 'Videos')
@section('content')
    <h3><i class="fa fa-tags"></i> Videos</h3>
    <div class="row mt">
        <div class="col-md-12">
            <div class="form-panel">
                <section id="no-more-tables">
                    <h4><i class="fa fa-table"></i> Video Lists <a href="{{route('post.index')}}" class="btn btn-success pull-right"><span class="fa fa-plus"></span> Add New Video</a></h4>
                    <hr>
                    @if(count($videos) > 0)
                        <table class="table table-striped table-condensed cf">
                            <thead class="cf">
                            <tr>
                                <th><i class="fa fa-bullhorn"></i> Title</th>
                                <th><i class="fa fa-question-circle"></i> Description</th>
                                <th><i class="fa fa-user"></i> Author</th>
                                <th><i class=" fa fa-calendar"></i> Date Created</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($videos as $video)
                                <tr>
                                    <td data-title="Title"><a href="{{route('video.edit',$video->id)}}">{{$video->title}}</a></td>
                                    <td data-title="Description">{!! str_limit($video->description,100,'...') !!}</td>
                                    <td data-title="Author">{{str_limit($video->user->name)}}</td>
                                    <td data-title="Date Created">{{date('M d, Y, D',strtotime($video->created_at))}}</td>
                                    <td>
                                        <a href="{{route('video.edit',$video->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil "></i></a>
                                        @if(count($videos) > 1)
                                            <a href="{{route('video.destroy',$video->id)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>No Results Found</p>
                    @endif
                </section>
            </div>
        </div>
    </div>
@stop
