@extends('layouts.admin')
@section('title', 'Posts')
@section('content')
    <h3><i class="fa fa-map-marker"></i> Virtual Tours</h3>
    <div class="row mt">
        <div class="col-md-12">
            <div class="form-panel">
                <section id="no-more-tables">
                    <h4><i class="fa fa-table"></i> Virtual Tour Lists <a href="{{route('virtual-tour.index')}}" class="btn btn-success pull-right"><span class="fa fa-plus"></span> Add New Virtual Tour</a></h4>
                    <hr>
                    @if(count($virtual_tours) > 0)
                        <table class="table table-striped table-condensed cf">
                            <thead class="cf">
                            <tr>
                                <th></th>
                                <th><i class="fa fa-location-arrow"></i> Location</th>
                                <th><i class="fa fa-map-marker"></i> Coordinates</th>
                                <th><i class=" fa fa-calendar"></i> Date Created</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($virtual_tours as $virtual_tour)
                                <tr>
                                    <td class="centered">
                                        <a href="{{route('post.edit',$virtual_tour->id)}}">
                                            <img src="{{$virtual_tour->featured_image ? asset($virtual_tour->featured_image) : asset('images/default/default.gif')}}" alt="{{$virtual_tour->name}}" class="img-circle" height="42" width="42">
                                        </a>
                                    </td>
                                    <td data-title="Location"><a href="{{route('virtual-tour.edit',$virtual_tour->id)}}">{{$virtual_tour->name}}</a></td>
                                    <td data-title="Coordinates">Longitude : {{$virtual_tour->longitude}} <br> Latitude : {{$virtual_tour->latitude}}</td>
                                    <td data-title="Date Created">{{date('M d, Y, D',strtotime($virtual_tour->created_at))}}</td>
                                    <td>
                                        <a href="{{route('virtual-tour.edit',$virtual_tour->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil "></i></a>
                                        @if(count($virtual_tours) > 1)
                                            <a href="{{route('virtual-tour.destroy',$virtual_tour->id)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>No Results Found</p>
                    @endif
                </section>
            </div>
        </div>
    </div>
@stop
