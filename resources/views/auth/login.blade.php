@extends('layouts.default')
@section('title', 'Login')
@section('content')
    <div class="row">
        <div class="col">
            <div class="col-md-12">
                {{Form::open(array('url' => 'login','id' => 'loginForm', 'class' => 'form-signin'))}}
                <h2 class="form-signin-heading">Please login</h2>
                <p>
                    {{Form::label('email','Your mail')}}
                    {{Form::text('email',Input::old('email'), array('placeholder' => 'email@site.com'))}}
                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </p>
                <p>
                    {{Form::label('password','Password')}}
                    {{Form::password('password')}}
                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </p>
                <p>
                    {{Form::submit('Login',array('id' => 'loginBtn'))}}
                </p>
                {{Form::close()}}
            </div>
        </div>
    </div>
@stop
