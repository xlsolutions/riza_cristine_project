@extends('layouts.default')
@section('title', 'Home')
@section('content')
    <div id="teaser" class="clearfix">

        <!--Overlay-->
        <div class="overlay">


            <!--Holder 960-->
            <div class="holder960 clearfix">

                <!--Teaser title-->
                <div class="teaserTitle">
                    <h1><span>light landing</span> your business</h1>
                    <h2>simple easy to use landing page </h2>
                </div>
                <!--End teaser title-->

                <!--Down-->
                <div class="down">
                    <a href="#about"><i class="icon-down-open-big"></i></a>
                </div>
                <!--End down-->

            </div>
            <!--End Holder 960-->

        </div>
        <!--End overlay-->
    </div>
@stop