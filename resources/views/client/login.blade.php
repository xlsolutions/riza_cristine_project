@extends('layouts.default')
@section('title', 'Login')
@section('content')
    <div style="margin-top:80px"></div>
    {{Form::open(array('url' => 'login','id' => 'loginForm', 'class' => 'form-signin'))}}
        <h2 class="form-signin-heading">Please login</h2>
        {{Form::text('email',Input::old('email'), array('placeholder' => 'Email Address','class' => 'form-control'))}}
        {{Form::password('password', array('placeholder' => 'Password','class' => 'form-control'))}}

        @if ($errors->any())
            <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <strong>{{ $error }}</strong><br>
            @endforeach
            </div>
        @endif
        {{Form::submit('Login',array('id' => 'loginBtn', 'class' => 'btn btn-lg btn-primary btn-block orange'))}}
    {{Form::close()}}
    <div style="margin-bottom:80px"></div>
@stop
