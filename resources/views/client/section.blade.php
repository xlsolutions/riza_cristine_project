<div id="home">
    <div id="bootstrap-touch-slider" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="5000" >

        <!-- Indicators -->
        <ol class="carousel-indicators">
            @foreach($announcements->slice(0, 5) as $key => $announcement)
                <li data-target="#bootstrap-touch-slider" data-slide-to="{{$key}}" class="{{$key == 0 ? 'active' : ''}}"></li>
            @endforeach
        </ol>

        <!-- Wrapper For Slides -->
        <div class="carousel-inner" role="listbox">
            @foreach($announcements as $key => $announcement)
                <div class="item {{$key == 0 ? 'active' : ''}}">

                    <!-- Slide Background -->
                    <img src="{{$announcement->featured_image}}" alt="{{$announcement->name}}"  class="slide-image"/>
                    <div class="bs-slider-overlay"></div>

                    <div class="container">
                        <div class="row">
                            <!-- Slide Text Layer -->
                            <div class="slide-text slide_style_center moreBtn">
                                <h1 data-animation="animated zoomInRight">{{$announcement->name}}</h1>
                                <p data-animation="animated fadeInLeft">{!! $announcement->description !!}</p>
                                <a href="#contact" class="btn btn-default {{$announcement->id % 2 == 0 ? 'orange' : ''}}" data-animation="animated fadeInLeft">Contact</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
            @endforeach
        </div><!-- End of Wrapper For Slides -->

        <!-- Left Control -->
        <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <!-- Right Control -->
        <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div> <!-- End  bootstrap-touch-slider Slider -->
</div>

<div class="video" id="videos">
    <div class="container">
        <div class="col-lg-12">
            <div class="text-center">
                <div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.3s">
                    <h2>Videos</h2>
                </div>
            </div>
            <div class="row">
                @foreach($videos as $key => $video)
                    <div class="col-md-3">
                        <a href="#{{$video->handle}}" data-toggle="modal" onclick="javascript: modalAutoPlay('{{$video->handle}}')">
                            <video width="100%" height="auto">
                                <source src="{{$video->featured_video}}" type="video/mp4">
                            </video>
                        </a>
                    </div>
                @endforeach
            </div>

            @foreach($videos as $key=>$video)
                <div id="{{$video->handle}}" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title">{{$video->title}}</h3>
                            </div>
                            <div class="modal-body">
                                <video width="100%" height="auto" controls id="video_{{$video->handle}}">
                                    <source src="{{$video->featured_video}}" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div id="post" class="blog">
    <div class="container">
        <div class="text-center">
            <div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.3s">
                <h2>Beach Lots</h2>
            </div>
            <div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.6s">
                <p>{{$siteSettings->site_subtitle['settings_value']}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="blog-item">
                    <div class="row">
                        @foreach($posts as $key=>$post)
                            <div class="col-xs-12 col-sm-2 {{($key % 2) ? 'pull-right' : ''}}">
                                <div class="entry-meta text-center">
                                    <span id="publish_date"><i class="fa fa-calendar"></i> {{date('d M',strtotime($post->created_at))}}</span>
                                    <span><i class="fa fa-user"></i> {{$post->user->name}}</span>
                                    <span><i class="fa fa-clock-o"></i> {{date('h:m A',strtotime($post->created_at))}}</span>
                                </div>
                                <div class="text-justify">
                                    <h3>{{$post->title}}</h3>
                                    <p>{!! str_limit($post->description,150,'...') !!}</p>
                                    <a class="btn btn-primary orange readmore" href="{{route('view.post',$post->handle)}}">Read More <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>

                            <div class="col-xs-12 {{$key % 2 === true ? 'col-sm-4' : 'col-sm-10'}} blog-content">
                                <a href="{{route('view.post',$post->handle)}}"><img class="img-responsive img-blog" src="{{$post->featured_image}}" alt="{{$post->title}}" width="100%" alt=""></a>
                            </div>
                        @endforeach
                    </div>
                </div><!--/.blog-item-->
            </div><!--/.col-md-8-->
        </div><!--/.row-->
    </div>
</div>

<div id="contact" class="contact">
    <div class="container">
        <div class="text-center">
            <div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.3s">
                <h2>Contact Us</h2>
            </div>
            <div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.6s">
                <p>{{$siteSettings->site_subtitle['settings_value']}}</p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-7">
        <div class="map">
            <div id="google-map" data-latitude="13.673040" data-longitude="121.394302"></div>
        </div>
    </div>
    <div class="contact-info">
        <div class="col-md-5">
            <h2>We are {{$siteSettings->site_name['settings_value']}}</h2>
            <h3>{{$siteSettings->site_subtitle['settings_value']}}</h3>
            <p>{{$siteSettings->site_contact_intro['settings_value']}}</p>
            <ul>
                <li><i class="fa fa-home fa-2x"></i> {{$siteSettings->site_address['settings_value']}} </li>
                <li><i class="fa fa-phone fa-2x"></i> {{$siteSettings->site_contact['settings_value']}}</li>
                <li><i class="fa fa-envelope fa-2x"></i> {{$siteSettings->site_email['settings_value']}}</li>
            </ul>
        </div>
    </div>
</div>

<div class="contact-form">
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>

            @if(Session::has('message'))
                <div class="alert alert-info">
                    {{Session::get('message')}}
                </div>
            @endif

            {!! Form::open(array('url' => 'contact', 'class' => 'form')) !!}

            <div class="form-group">
                {!! Form::text('name', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Your name')) !!}
            </div>

            <div class="form-group">
                {!! Form::text('email', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Your e-mail address')) !!}
            </div>

            <div class="form-group">
                {!! Form::text('subject', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Subject')) !!}
            </div>

            <div class="form-group">
                {!! Form::textarea('message', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Your message')) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Contact Us!',
                  array('class'=>'btn btn-primary orange')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div id="left">
    <a class="handle ui-slideouttab-handle-rounded">PROMO<i class="fa fa-icon fa-television"></i></a>
    <p style="font-weight: bold;">
        {{$siteSettings->site_promo['settings_value']}}
    </p>
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>

    {!! Form::open(array('url' => 'promo', 'class' => 'form')) !!}
    <div class="form-group">
        {!! Form::text('name', null,
            array('required',
                  'class'=>'form-control',
                  'placeholder'=>'Your name')) !!}
    </div>
    <div class="form-group">
        {!! Form::text('contact', null,
            array('required',
                  'class'=>'form-control',
                  'placeholder'=>'Contact')) !!}
    </div>
    <div class="form-group">
        {!! Form::text('email', null,
            array('required',
                  'class'=>'form-control',
                  'placeholder'=>'Your e-mail address')) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('INVEST NOW!',
          array('class'=>'btn btn-primary orange')) !!}
    </div>
    {!! Form::close() !!}
</div>