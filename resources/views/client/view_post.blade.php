@extends('layouts.parallax')
@section('title', 'Beach Lots')
@section('content')
    <div id="post">
        <div class="bs-slider control-round indicators-line" >
            <img src="../{{$post->featured_image ? $post->featured_image : asset('images/default/default.gif')}}" alt="{{$post->title}}"  class="slide-image"/>
            <div class="bs-slider-overlay"></div>

            <div class="container">
                <div class="row">
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_center">
                        <h1 class="wow zoomInRight">{{$post->title}}</h1>
                        <p><strong class="wow fadeInLeft">{!! $post->user->name !!}</strong> <strong class="wow bounceInDown">|</strong> <strong class="wow fadeInRight">{{date('M d, Y',strtotime($post->created_at))}}</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="text-center">
                    <div class="col-md-10 center-block">
                        <div class="wow bounceInLeft" data-wow-offset="0" data-wow-delay="0.3s">
                            <h2>{{$post->title}}</h2>
                        </div>
                    </div>
                </div>
                <div class="text-justify">
                    <div class="col-md-10 post-bottom-buffer center-block">
                        <div class="wow bounceInLeft" data-wow-offset="0" data-wow-delay="0.6s">
                            <p style="color:#000!important;">{!! $post->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 pull-right">
                <div class="text-center">
                    <div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.3s">
                        <h2>Contact Us</h2>
                    </div>
                </div>

                <div class="contact-info">
                    <ul>
                        <li><i class="fa fa-home fa-2x"></i> {{$siteSettings->site_address['settings_value']}} </li>
                        <li><i class="fa fa-phone fa-2x"></i> {{$siteSettings->site_contact['settings_value']}}</li>
                        <li><i class="fa fa-envelope fa-2x"></i> {{$siteSettings->site_email['settings_value']}}</li>
                    </ul>
                </div>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>

                @if(Session::has('message'))
                    <div class="alert alert-info">
                        {{Session::get('message')}}
                    </div>
                @endif

                {!! Form::open(array('url' => 'contact', 'class' => 'form')) !!}

                <div class="form-group">
                    {!! Form::text('name', null,
                        array('required',
                              'class'=>'form-control',
                              'placeholder'=>'Your name')) !!}
                </div>

                <div class="form-group">
                    {!! Form::text('email', null,
                        array('required',
                              'class'=>'form-control',
                              'placeholder'=>'Your e-mail address')) !!}
                </div>

                <div class="form-group">
                    {!! Form::text('subject', null,
                        array('required',
                              'class'=>'form-control',
                              'placeholder'=>'Subject')) !!}
                </div>

                <div class="form-group">
                    {!! Form::textarea('message', null,
                        array('required',
                              'class'=>'form-control',
                              'placeholder'=>'Your message')) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Contact Us!',
                      array('class'=>'btn btn-primary orange')) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        </div>
    </div>
@stop