@extends('layouts.parallax')
@section('title', 'LIFE IN COLOR')
@section('content')
    <div class="container">
        <div class="row post-bottom-buffer">
            <div class="map">
                <h4>{{$virtual_tour->name}}</h4>
                <div class="pano" id="street-view" data-lat="{{$virtual_tour->latitude}}" data-lng="{{$virtual_tour->longitude}}"></div>
            </div>
        </div>
    </div>
@stop