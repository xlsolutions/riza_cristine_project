@extends('layouts.parallax')
@section('title', 'LIFE IN COLOR')
@section('content')
    <div class="text-center">
        <div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.3s">
            <h2>Virtual Tours</h2>
        </div>
        <div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.6s">
            <p>{{$siteSettings->site_subtitle['settings_value']}}</p>
        </div>
    </div>
    <div class="container">
        <div class="col-sm-12">
            <div class="col-xs-12">
            @foreach($virtual_tours as $key=>$virtual_tour)
                <div class="col-sm-6">
                    <div class="blog-content">
                        <div class="blog-item">
                            <a href="{{route('view.tour',$virtual_tour->handle)}}"><img class="img-responsive img-blog" src="{{$virtual_tour->featured_image ? $virtual_tour->featured_image : asset('images/default/default.gif')}}" width="100%" alt=""></a>
                            <h2>{{$virtual_tour->name}}</h2>
                            <a class="btn btn-primary orange readmore" href="{{route('view.tour',$virtual_tour->handle)}}">View Tour <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
@stop