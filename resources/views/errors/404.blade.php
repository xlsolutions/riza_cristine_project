@extends('layouts.default')
@section('title', '404 Page not found')
@section('content')
    <section class="clearfix section">

        <!--Holder 960-->
        <div class="holder960 clearfix">

            <!--Section title-->
            <div class="secArrow">
                <div class="arrowHolder"><img src="images/bigArrow-1.png" alt=""></div>
                <i class="icon-user"></i>
            </div>
            <!--End section title-->


            <!--Intro-->
            <div class="loginIntro">
                <h1><span class="black">404</span>
                    Page not found</h1>
            </div>
            <!--End intro-->

        </div>
        <!--End Holder 960-->

    </section>
    <section id="shareon" class="section">


        <!--Holder 960-->
        <div class="holder960 clearfix">


            <!--Section title-->
            <div class="secArrow">
                <div class="arrowHolder"><img src="{{asset('themes/orange/images/bigArrow-3.png')}}" alt=""></div>
                <i class="icon-share"></i>
            </div>
            <!--End section title-->


            <!--Intro-->
            <div class="shareIntro">
                <h1><span class="black">keep it calm</span> and share on</h1>



            {{--<!--Socials share-->--}}
            {{--<ul class="socialsShare">--}}

            {{--<li><a class="socialTwitter" href="#"><i class="icon-twitter"></i></a></li>--}}
            {{--<li><a class="socialFacebook" href="#"><i class="icon-facebook"></i></a></li>--}}
            {{--<li><a class="socialGplus" href="#"><i class="icon-gplus"></i></a></li>--}}
            {{--<li><a class="socialPinterest" href="#"><i class="icon-pinterest"></i></a></li>--}}
            {{--<li><a class="socialLinkedin" href="#"><i class="icon-linkedin"></i></a></li>--}}
            {{--<li><a class="socialDribbble" href="#"><i class="icon-dribbble"></i></a></li>--}}
            {{--<li><a class="socialVimeo" href="#"><i class="icon-vimeo"></i></a></li>--}}
            {{--<li><a class="socialFilckr" href="#"><i class="icon-flickr"></i></a></li>--}}



            {{--</ul>--}}
            <!--End socials share-->
            </div>
            <!--End intro-->


        </div>
        <!--End Holder 960-->

    </section>
@stop