<!--footer start-->
<footer class="site-footer">
    <div class="text-center">
        {{ current_year() }} - {{$siteSettings->site_footer['settings_value']}}
        <a href="#" class="go-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
</footer>
<!--footer end-->