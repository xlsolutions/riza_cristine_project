<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title') | {{config('app.name')}}</title>

<!-- Bootstrap core CSS -->
<link href="{{asset("css/bootstrap.css")}}" rel="stylesheet">
<!--external css-->
<link href="{{asset("font-awesome/css/font-awesome.css")}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{asset("css/zabuto_calendar.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("js/gritter/css/jquery.gritter.css")}}" />
<link rel="stylesheet" type="text/css" href="{{asset("lineicons/style.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("js/bootstrap-datepicker/css/datepicker.css")}}" />
<link rel="stylesheet" type="text/css" href="{{asset("js/bootstrap-daterangepicker/daterangepicker.css")}}" />

<!-- Custom styles for this template -->
<link href="{{asset("css/style.css")}}" rel="stylesheet">
<link href="{{asset("css/style-responsive.css")}}" rel="stylesheet">
<link href="{{asset("css/table-responsive.css")}}" rel="stylesheet">
<link href="{{asset("css/admin-custom.css")}}" rel="stylesheet">

<script src="{{asset("js/chart-master/Chart.js")}}"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->