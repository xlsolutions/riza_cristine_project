<!--header start-->
<header class="header black-bg">
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
    </div>
    <!--logo start-->
    <a href="/" class="logo"><b>{{$siteSettings->site_name['settings_value']}} </b></a>
    <!--logo end-->
    <div class="top-menu">
        <ul class="nav pull-right top-menu">
            <li>
                <a class="logout" href="/">
                    Home
                </a>
            </li>
            <li>
                <a class="logout" href="logout"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    Logout
                </a>
            </li>

            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
            </form>
        </ul>
    </div>
</header>
<!--header end-->