<!-- js placed at the end of the document so clientages load faster -->
<script src="{{asset("js/jquery.js")}}"></script>
<script src="{{asset("js/jquery-1.8.3.min.js")}}"></script>
<script src="{{asset("js/bootstrap.min.js")}}"></script>
<script class="include" type="text/javascript" src="{{asset("js/jquery.dcjqaccordion.2.7.js")}}"></script>
<script src="{{asset("js/jquery.scrollTo.min.js")}}"></script>
<script src="{{asset("js/jquery.nicescroll.js")}}" type="text/javascript"></script>
<script src="{{asset("js/jquery.sparkline.js")}}"></script>


<!--common script for clientages-->
<script src="{{asset("js/common-scripts.js")}}"></script>

<script type="text/javascript" src="{{asset("js/gritter/js/jquery.gritter.js")}}"></script>
<script type="text/javascript" src="{{asset("js/gritter-conf.js")}}"></script>

<!--script for this page-->
<script src="{{asset("js/sparkline-chart.js")}}"></script>
<script src="{{asset("js/zabuto_calendar.js")}}"></script>

<!--custom switch-->
<script src="{{asset("js/bootstrap-switch.js")}}"></script>

<!--custom tagsinput-->
<script src="{{asset("js/jquery.tagsinput.js")}}"></script>

<!--custom checkbox & radio-->

<script type="text/javascript" src="{{asset("js/bootstrap-datepicker/js/bootstrap-datepicker.js")}}"></script>
<script type="text/javascript" src="{{asset("js/bootstrap-daterangepicker/daterangepicker.js")}}"></script>

<script type="text/javascript" src="{{asset("js/bootstrap-inputmask/bootstrap-inputmask.min.js")}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhth3ecxBfu1jOOQ3Wmzp5VboR30_O0kg&sensor=false&libraries=places"></script>

<script src="{{asset("js/tinymce/tinymce.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        tinymce.init({
            selector: "textarea",
            theme: "modern",
            paste_data_images: true,
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor emoticons",
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
                if (meta.filetype == 'image') {
                    $('#upload').trigger('click');
                    $('#upload').on('change', function() {
                        var file = this.files[0];
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            callback(e.target.result, {
                                alt: ''
                            });
                        };
                        reader.readAsDataURL(file);
                    });
                }
            },
            templates: [{
                title: 'Test template 1',
                content: 'Test 1'
            }, {
                title: 'Test template 2',
                content: 'Test 2'
            }]
        });
    });
</script>

<script type="text/javascript">
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });
</script>


<script>
    /* script */
    function initialize() {
        var latlng = new google.maps.LatLng(14.5831,120.9794);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 13
        });
        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });

        var input = document.getElementById('location');
        var geocoder = new google.maps.Geocoder();
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                console.log("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
            infowindow.setContent(place.formatted_address);
            infowindow.open(map, marker);

        });
        // this function will work on marker move event into map
        google.maps.event.addListener(marker, 'dragend', function() {
            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    }
    function bindDataToForm(address,lat,lng){
        document.getElementById('name').value = address;
        document.getElementById('latitude').value = lat;
        document.getElementById('longitude').value = lng;
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>