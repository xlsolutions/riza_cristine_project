<!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <h5 class="centered">
                {{$currUser->name }}
            </h5>

            {{--<li class="mt">
                <a class="{{ Request::path() === 'admin/dashboard' ? 'active' : '' }}" href="{{route('admin.dashboard')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>--}}

            <li class="sub-menu">
                <a class="{{ Request::path() === 'admin/announcements' ? 'active' : '' }}" href="{{route('admin.announcements')}}">
                    <i class="fa fa-bullhorn"></i>
                    <span>Announcements</span>
                </a>
            </li>

            <li class="sub-menu">
                <a class="{{ Request::path() === 'admin/posts' ? 'active' : '' }}" href="{{route('admin.posts')}}">
                    <i class="fa fa-tags"></i>
                    <span>Posts</span>
                </a>
            </li>

            <li class="sub-menu">
                <a class="{{ Request::path() === 'admin/videos' ? 'active' : '' }}" href="{{route('admin.videos')}}">
                    <i class="fa fa-video-camera"></i>
                    <span>Videos</span>
                </a>
            </li>

            <li class="sub-menu">
                <a class="{{ Request::path() === 'admin/users' ? 'active' : '' }}" href="{{route('admin.users')}}">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                </a>
            </li>

            <li class="sub-menu">
                <a class="{{ Request::path() === 'admin/virtual-tours' ? 'active' : '' }}" href="{{route('admin.virtual-tours')}}">
                    <i class="fa fa-map-marker"></i>
                    <span>Virtual Tour Settings</span>
                </a>
            </li>

            <li class="sub-menu">
                <a class="{{ Request::path() === 'admin/site-settings' ? 'active' : '' }}" href="{{route('admin.site-settings')}}">
                    <i class="fa fa-cog"></i>
                    <span>Site Settings</span>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->