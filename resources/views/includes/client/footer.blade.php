<div class="footer">
    <div class="container">
        <div class="col-md-4 wow fadeInUp" data-wow-offset="0" data-wow-delay="0.2s">
            <h2>{{$siteSettings->site_name['settings_value']}}</h2>
            <p>{{$siteSettings->site_subtitle['settings_value']}}</p>

            <ul class="social-network">
                <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook fa-1x"></i></a></li>
                <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter fa-1x"></i></a></li>
                <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin fa-1x"></i></a></li>
                <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest fa-1x"></i></a></li>
                <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus fa-1x"></i></a></li>
            </ul>
        </div>

        <div class="col-md-4 wow fadeInUp" data-wow-offset="0" data-wow-delay="0.4s">
            <h3>RECENT POSTS</h3>
            <ul>
                @foreach($posts as $key=>$post)
                    <li><a class="footer-post" href="#">{{$post->title}}</a></li>
                @if($key+1 < count($posts))
                    <hr>
                @endif
                @endforeach
            </ul>
        </div>

        <div class="col-md-4 wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
            <h3>CONTACT INFO</h3>
            <ul>
                <li><i class="fa fa-home fa-2x"></i> {{$siteSettings->site_address['settings_value']}}</li><hr>
                <li><i class="fa fa-phone fa-2x"></i> {{$siteSettings->site_contact['settings_value']}}</li><hr>
                <li><i class="fa fa-envelope fa-2x"></i> {{$siteSettings->site_email['settings_value']}}</li>
            </ul>
        </div>

    </div>
</div>

<div class="sub-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                &copy; {{$siteSettings->site_footer['settings_value']}} {{ current_year() }}. All Rights Reserved.
            </div>
            <div class="col-md-6">
                <ul class="pull-right">
                    <li><a href="/#home">Home</a></li>
                    <li><a href="/#aboutus">About Us</a></li>
                    <li><a href="/#gallery">Gallery</a></li>
                    <li><a href="/virtual-tour">Virtual Tour</a></li>
                    <li><a href="/#contact">Contact Us</a></li>
                </ul>
            </div>
        </div>
        <div class="pull-right">
            <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
        </div>
    </div>
</div>