<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="{{$siteSettings->site_subtitle['settings_value']}},
                                    A QUALITY INVESTMENT and QUALITY TIME YOU CAN SHARE TO YOUR FAMILY!
                                    Have your Own Exclusive Beach Lot only here @ Playa Laiya Juan, Batangas">
<meta name="author" content="{{$siteSettings->site_name['settings_value']}}">
<meta name="keywords" content="Beachlot, Playa, San Juan, Batangas, Laiya, playalaiyabeachlot,
                                Playa Laiya, Beach, Investment, beach side,Laiya Beachlot">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title') | {{$siteSettings->site_name['settings_value']}}</title>

<!-- load bootstrap from a cdn -->
<link rel="stylesheet" href="{{asset('themes/multi/css/bootstrap.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('themes/multi/css/style.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('themes/multi/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('themes/multi/css/isotope.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('themes/multi/css/animate.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('themes/multi/js/fancybox/jquery.fancybox.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('themes/multi/css/prettyPhoto.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('themes/multi/css/custom.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('themes/multi/css/custom-slider.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('css/tabSlideOut.css')}}" type="text/css"/>