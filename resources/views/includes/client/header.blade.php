<div class=" clearfix">
    <header>
        <nav class="navbar navbar-default navbar-fixed-top navbar-transparent" role="navigation">
            <div class="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-brand">
                            <a href="/#home"><h1>{{$siteSettings->site_name['settings_value']}} </h1></a>
                        </div>
                    </div>

                    <div class="navbar-collapse collapse">
                        <div class="menu" id="mainNav">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/#home">Home</a></li>
                                <li role="presentation"><a href="/#videos">Video</a></li>
                                <li role="presentation"><a href="/#post">Latest</a></li>
                                <li role="presentation"><a href="/#contact">Contact us</a></li>
                                <li role="presentation"><a href="{{ route('virtual.tour') }}">Virtual Tour</a></li>
                                @if (isset($isAdmin))
                                    <li role="presentation"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
</div>