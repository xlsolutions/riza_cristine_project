<nav id="mainNav" >
    <a href="#" class="mobileBtn" ><i class="icon-menu"></i></a>
    <ul>
        <li><a href="/#home">Home</a></li>
        <li><a href="/#about">About us</a></li>
        <li><a href="/#gallery">Gallery</a></li>
        <li><a href="{{ route('virtual.tour') }}">Virtual Tour</a></li>
        <li><a href="/#newsletter">Subscribe</a></li>
        <li><a href="/#faq">FAQ</a></li>
        <li><a href="/#contact">Contact us</a></li>
        @if (Auth::guest())
            <li><a href="{{ route('login') }}">Login</a></li>
        @else
            @if ($isAdmin === true)
                <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            @endif
            <li>
                <a href="logout"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                </form>
            </li>
        @endif
    </ul>
</nav>