<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="{{asset('themes/multi/js/jquery-2.1.1.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individgual files as needed -->
<script type="text/javascript" src="{{asset('themes/multi/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/wow.min.js')}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoUB8QBHGvri1pmt14TmivcxBDctwCGFI"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/fancybox/jquery.fancybox.pack.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/jquery.bxslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/jquery.prettyPhoto.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/jquery.isotope.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/functions.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/multi/js/custom-slider.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tabSlideOut.js')}}"></script>

<script>
    $(function(){
        wow = new WOW({}).init();

        $('#left').tabSlideOut({
            tabLocation: 'left',
            clickScreenToClose: false,
            offset: '100px',
            offsetReverse: true,
            onLoadSlideOut: false,
            outTime: 2000,

            // handlers: enable and disable buttons when sliding open or closed
            onOpen: function(){
                $('#open').prop('disabled',true);
                $('#close').prop('disabled',false);

                setInterval(function () {
                    $('#left').tabSlideOut('close');
                },15000);
            },
            onClose: function(){
                $('#open').prop('disabled',false);
                $('#close').prop('disabled',true);
            }
        });

        $(window).scroll(function() {
            var height = $(window).scrollTop();

            if(height > $("#post").height())
            {
                $('#left').tabSlideOut('open');
            }
        });

        /* programmatically drive the left tab */
        $('button').click(function(event){
            left.tabSlideOut( /*command*/$(event.target).attr('id') );
        });

        /* register event handler */
        $(document).on('slideouttabopen slideouttabclose slideouttabbounce',function(event){
            var text = $(event.target).attr('id')+': '+event.type;
            $('#events').append(text+"\n");
        });

        var panoramas = [];

        panoDivs = document.getElementsByClassName('pano');

        $(".pano").each(function(idx, el) {
            var panorama = new google.maps.StreetViewPanorama(
                document.getElementById('street-view'),
                {
                    position: {lat: parseFloat($(this).data("lat")), lng: parseFloat($(this).data("lng"))},
                    pov: {heading: 165, pitch: 0},
                    zoom: 1
                });

            panoramas.push(panorama);
        });
    });

    function modalAutoPlay(handle) {
        console.log(handle);
        $('#'+handle).on('shown.bs.modal', function () {
            $('#video_'+handle)[0].play();
        });
        $('#'+handle).on('hidden.bs.modal', function () {
            $('#video_'+handle)[0].pause();
        });
    }
</script>