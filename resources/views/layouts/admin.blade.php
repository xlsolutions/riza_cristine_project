<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.admin.head')
</head>
<body>
<section id="container" >

    @include('includes.admin.header')

    @include('includes.admin.sidebar')

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            @yield('content')
            <input name="image" type="file" id="upload" style="display: none;" onchange="">
        </section>
    </section>
    <!--main content end-->

    @include('includes.admin.footer')

</section>

@include('includes.admin.scripts')
</body>
</html>
