<!doctype html>
<html>
<head>
    @include('includes.client.head')
</head>
<body>
    <header id="header" class=" default clearfix">
        @include('includes.client.header')
    </header>

    <div class="wrapper">

        @yield('content')

    </div>

    <footer id="footer" class="clearfix">
        @include('includes.client.footer')
    </footer>

@include('includes.client.scripts')
</body>
</html>
