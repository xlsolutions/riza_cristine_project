<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="wrapper">

    <header id="header" class=" default clearfix">
        @include('includes.header')
    </header>

    @include('client.sidebar')

    @yield('content')

    <footer id="footer" class="clearfix">
        @include('includes.footer')
    </footer>

</div>

@include('includes.scripts')
</body>
</html>
