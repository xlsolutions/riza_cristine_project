<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', array('uses' => 'HomeController@index'));



Route::group(['prefix' => 'virtual-tour'], function(){
    Route::get('/', array('uses' => 'HomeController@showVirtualTour'))->name('virtual.tour');
    Route::get('/{handle}',array('uses' => 'Client\VirtualTourController@viewVirtualTour'))->name('view.tour');
});

Route::group(['prefix' => 'post'], function(){
    Route::get('/{handle}',array('uses' => 'Client\PostController@viewPost'))->name('view.post');
});

Route::auth();

Route::group(['prefix' => 'admin','middleware' => 'admin'], function(){
    Route::get('/dashboard', 'Admin\AdminController@showAnnouncements')->name('admin.dashboard');
    Route::get('/announcements','Admin\AdminController@showAnnouncements')->name('admin.announcements');
    Route::get('/posts','Admin\AdminController@showPosts')->name('admin.posts');
    Route::get('/videos','Admin\AdminController@showVideos')->name('admin.videos');
    Route::get('/users','Admin\AdminController@showUsers')->name('admin.users');
    Route::get('/site-settings','Admin\AdminController@showSiteSettings')->name('admin.site-settings');
    Route::get('/virtual-tours','Admin\AdminController@showVirtualSettings')->name('admin.virtual-tours');

    Route::group(['namespace' => 'Admin','middleware' => 'admin'], function(){
        Route::resource('user','UserResource');
        Route::get('user/{user}/delete', [
            'as' => 'user.destroy',
            'uses' => 'UserResource@destroy'
        ]);

        Route::resource('announcement','AnnouncementResource');
        Route::get('announcement/{user}/delete', [
            'as' => 'announcement.destroy',
            'uses' => 'AnnouncementResource@destroy'
        ]);

        Route::resource('post','PostResource');

        Route::resource('video','VideoResource');

        Route::resource('site-setting','SiteSettingsResource');

        Route::resource('virtual-tour','VirtualTourResource');
    });
});

Route::resource('contact','ContactController');
Route::resource('promo','PromoController');